<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');

Route::get('produksi/json', 'ProductionController@json');
Route::get('produksi/chart_data', 'ProductionController@chart_data');
Route::resource('produksi', 'ProductionController');

Route::post('regresi/proses', 'RegressionController@prediction_process');

Route::get('dashboard', 'UserController@index');

Route::get('pengaturan/end_year_list/{start_year}', 'SettingController@end_year_list');
Route::resource('pengaturan', 'SettingController');

