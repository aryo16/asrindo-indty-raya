<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regression extends Model
{
    protected $y, $alpha, $beta_1, $beta_2, $beta_3;

    /**
     * Regression constructor.
     * @param $y
     * @param $alpha
     * @param $beta_1
     * @param $beta_2
     * @param $beta_3
     */
    public function __construct($y, $alpha, $beta_1, $beta_2, $beta_3)
    {
        $this->y = $y;
        $this->alpha = $alpha;
        $this->beta_1 = $beta_1;
        $this->beta_2 = $beta_2;
        $this->beta_3 = $beta_3;
    }

    /**
     * @return array
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @return mixed
     */
    public function getAlpha()
    {
        return $this->alpha;
    }

    /**
     * @return mixed
     */
    public function getBeta1()
    {
        return $this->beta_1;
    }

    /**
     * @return mixed
     */
    public function getBeta2()
    {
        return $this->beta_2;
    }

    /**
     * @return mixed
     */
    public function getBeta3()
    {
        return $this->beta_3;
    }



}
