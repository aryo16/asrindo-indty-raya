<?php

namespace App\Http\Controllers;

use anlutro\LaravelSettings\SettingsManager;
use anlutro\LaravelSettings\SettingStore;
use anlutro\LaravelSettings\Facade as Setting;
use App\Prediction;
use App\Production;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        Setting::set('alpha', null);
//        Setting::set('beta_1', null);
//        Setting::set('beta_2', null);
//        Setting::set('beta_3', null);
        //mendapatkan model regresi
        $alpha = \setting('alpha', null);
        $beta_1 = \setting('beta_1', null);
        $beta_2 = \setting('beta_2', null);
        $beta_3 = \setting('beta_3', null);


        //mendapatkan data tahun produksi yang tersimpan pada database
        $year = Production::all()
            ->pluck('year', 'year');

        $count_data = count($year);

        if($count_data == 0){
            //tidak ada data
            Session::flash('message_query', 'Kesalahan!');
            Session::flash('message_header', 'Kesalahan!');
            Session::flash('message', 'Tidak ada data produksi tersimpan. Masukkan terlebih dahulu data produksi.');
            Session::flash('icon', 'pe-7s-close-circle');
            Session::flash('class-style', 'alert-danger');
            return view('setting.index', compact('alpha', 'beta_1', 'beta_2', 'beta_3'));
        }else{
            $start_year = setting('start_year', null);
            return view('setting.index', compact('year', 'start_year', 'alpha', 'beta_1', 'beta_2', 'beta_3'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            Setting::set('start_year', $request->start_year);
            Setting::set('end_year', $request->end_year);
            Setting::save();

            //flash message
            Session::flash('message_setting', 'yes');
            Session::flash('message_header', 'Berhasil!');
            Session::flash('message', 'Pengaturan berhasil disimpan.');
            Session::flash('icon', 'pe-7s-check');
            Session::flash('class-style', 'alert-success');
        }catch (\Exception $e){

            //tidak ada data
            Session::flash('message_setting', 'yes');
            Session::flash('message_header', 'Kesalahan!');
            Session::flash('message', 'Pengaturan gagal disimpan.');
            Session::flash('icon', 'pe-7s-close-circle');
            Session::flash('class-style', 'alert-danger');
        }

        return redirect('pengaturan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function end_year_list($start_year){
        $end_years = Production::where('year', '>=', $start_year)
            ->select(['year'])
            ->get();

        $response = '';

        //get value end_year Setting
        $end_year_setting = setting('end_year', null);

        foreach ($end_years as $end_year){
            if($end_year->year == $end_year_setting){
                $data = '<option value="'.$end_year->year.'" selected>'.$end_year->year.'</option>';
            }else{
                $data = '<option value="'.$end_year->year.'">'.$end_year->year.'</option>';
            }
            $response= $response.$data;
        }

        return $response;
    }
}
