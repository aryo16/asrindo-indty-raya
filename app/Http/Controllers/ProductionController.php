<?php

namespace App\Http\Controllers;

use App\Production;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ProductionController extends Controller
{
    public function index()
    {
        return view('production.index');
    }

    public function store(Request $request)
    {
        try{

            $alpha = setting('alpha', null);
            $beta_1 = setting('beta_1', null);
            $beta_2 = setting('beta_2', null);
            $beta_3 = setting('beta_3', null);

            if($alpha != null && $beta_1 != null && $beta_2 != null && $beta_3 != null){
                $request->merge([
                    'prediction' => $alpha + ($beta_1 * $request->employee) + ($beta_2 * $request->machine) + ($beta_3 * $request->demand)
                ]);
            }

            Production::create($request->input());

            //flash message
            Session::flash('message_query', 'yes');
            Session::flash('message_header', 'Berhasil!');
            Session::flash('message', 'Data produksi berhasil disimpan.');
            Session::flash('icon', 'pe-7s-check');
            Session::flash('class-style', 'alert-success');
        }catch (\PDOException $e){
            //flash message
            Session::flash('message_query', 'yes');
            Session::flash('message_header', 'Gagal!');
            Session::flash('message', 'Data produksi gagal disimpan.'.$e->getMessage());
            Session::flash('icon', 'pe-7s-close-circle');
            Session::flash('class-style', 'alert-danger');
        }

        return redirect('produksi');
    }

    public function show($id)
    {
        $production = Production::find($id);
        return response()->json($production);
    }

    public function update(Request $request, $id)
    {
        try{
            $production = Production::find($id);
            $production->year = $request->year;
            $production->machine = $request->machine;
            $production->employee = $request->employee;
            $production->demand = $request->demand;
            $production->production = $request->production;

            $alpha = setting('alpha', null);
            $beta_1 = setting('beta_1', null);
            $beta_2 = setting('beta_2', null);
            $beta_3 = setting('beta_3', null);

            if($alpha != null && $beta_1 != null && $beta_2 != null && $beta_3 != null){
                $production->prediction = $alpha + ($beta_1 * $request->employee) + ($beta_2 * $request->machine) + ($beta_3 * $request->demand);
            }

            $production->save();

            //flash message
            Session::flash('message_query', 'yes');
            Session::flash('message_header', 'Berhasil!');
            Session::flash('message', 'Data produksi berhasil diperbarui.');
            Session::flash('icon', 'pe-7s-check');
            Session::flash('class-style', 'alert-success');

        }catch (\PDOException $e){
            //flash message
            Session::flash('message_query', 'yes');
            Session::flash('message_header', 'Gagal!');
            Session::flash('message', 'Data produksi gagal diperbarui.');
            Session::flash('icon', 'pe-7s-close-circle');
            Session::flash('class-style', 'alert-danger');
        }

        return redirect('produksi');
    }

    public function destroy($id)
    {
        $start_year = setting('start_year', null);
        $end_year = setting('end_year', null);

        $year_to_delete = Production::find($id)->select(['year']);

        if($year_to_delete <= $end_year && $year_to_delete >= $start_year){
            Production::destroy($id);
        }
    }

    public function json(){
        $productions = Production::all();

        return DataTables::of($productions)
            ->addColumn('action', function ($production){
                return "
                    <a class='btn btn-warning btn-xs btnModalEditProduction waves-effect' id='$production->id'>EDIT</a>
                    <a class='btn btn-danger btn-xs btnDeleteProduction waves-effect' id='$production->id'>DELETE</a>
                ";
            })
            ->make('true');
    }

    public function chart_data(){
        $datas = Production::select([
            'production',
            'prediction',
            'year'
        ])
            ->get();

        $productions = array();
        $predictions = array();
        $year = array();

        foreach ($datas as $data){
            array_push($productions, $data->production);
            if($data->prediction == null){
                array_push($predictions, 0);
            }else{
                array_push($predictions, $data->prediction);
            }
            array_push($year, $data->year);

        }

        $response = [
            'production' => $productions,
            'prediction' => $predictions,
            'label' => ['Produksi', 'Prediksi'],
            'year' => $year
        ];

        return response()->json($response);
    }
}
