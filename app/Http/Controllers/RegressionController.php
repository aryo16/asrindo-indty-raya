<?php

namespace App\Http\Controllers;

use App\Production;
use App\Regression;
use Illuminate\Http\Request;
use anlutro\LaravelSettings\Facade as Setting;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Phpml\Exception\MatrixException;
use Phpml\Regression\LeastSquares;
use PHPUnit\Runner\Exception;

class RegressionController extends Controller
{
    public function prediction_process_lib(){
        try{
            //mendapatkan data start_year dan end_year pada setting
            $start_year = setting('start_year', null);
            $end_year = setting('end_year', null);

            $productions = Production::whereBetween('year', [$start_year, $end_year])
                ->get();

            $train_data = array();
            $target_data = array();
            foreach ($productions as $production){
                $data = [$production->employee, $production->machine, $production->demand];
                array_push($train_data, $data);
                array_push($target_data, $production->production);
            }

            $regression = new LeastSquares();
            $regression->train($train_data, $target_data);

            //save alpha
            Setting::set( 'alpha',$regression->getIntercept());
            //beta
            $beta = $regression->getCoefficients();
            //save beta_1
            Setting::set('beta_1', $beta[0]);
            //save beta_2
            Setting::set('beta_2', $beta[1]);
            //save beta_3
            Setting::set('beta_3', $beta[2]);
            //save setting
            Setting::save();

            //update prediction value
            $productions = Production::all();

            foreach ($productions as $production){
                $prediction = $regression->getIntercept()
                    + ($beta[0] * $production->employee)
                    + ($beta[1] * $production->machine)
                    + ($beta[2] * $production->demand);
                //update to db
                $update_pred = Production::find($production->id);
                $update_pred->prediction = $prediction;
                $update_pred->save();
            }

            //flash message
            Session::flash('message_regression', 'yes');
            Session::flash('message_header', 'Berhasil!');
            Session::flash('message', 'Model regresi linear berganda berhasil dibuat.');
            Session::flash('icon', 'pe-7s-check');
            Session::flash('class-style', 'alert-success');


        }catch (MatrixException $e){
            //flash message
            Session::flash('message_regression', 'yes');
            Session::flash('message_header', 'Kesalahan!');
            Session::flash('message', 'Gagal membuat model regresi linear berganda, gunakan jangka tahun lain.');
            Session::flash('icon', 'pe-7s-close-circle');
            Session::flash('class-style', 'alert-danger');
        }

        return redirect('pengaturan');
    }

    public function prediction_process(){
        try{
            //mendapatkan data start_year dan end_year pada setting
            $start_year = setting('start_year', null);
            $end_year = setting('end_year', null);

            $productions = Production::whereBetween('year', [$start_year, $end_year])
                ->get();

//            $train_data = array();
//            $target_data = array();
//            foreach ($productions as $production){
//                $data = [$production->employee, $production->machine, $production->demand];
//                array_push($train_data, $data);
//                array_push($target_data, $production->production);
//            }

            $sum_x1 = 0;
            $sum_x2 = 0;
            $sum_x3 = 0;
            $sum_y = 0;
            $sum_x1_y = 0;
            $sum_x2_y = 0;
            $sum_x3_y = 0;
            $sum_x1_x1 = 0;
            $sum_x1_x2 = 0;
            $sum_x1_x3 = 0;
            $sum_x2_x2 = 0;
            $sum_x2_x3 = 0;
            $sum_x3_x3 = 0;
            $count_data = count($productions);
            $hasil_y = 0;
            $hasil_alpha = 0;
            $hasil_beta_1 = 0;
            $hasil_beta_2 = 0;
            $hasil_beta_3 = 0;

            foreach ($productions as $production){
                $sum_x1 += $production->employee;
                $sum_x2 += $production->machine;
                $sum_x3 += $production->demand;
                $sum_y += $production->production;
                $sum_x1_y += ($production->employee * $production->production);
                $sum_x2_y += ($production->machine * $production->production);
                $sum_x3_y += ($production->demand * $production->production);
                $sum_x1_x1 += ($production->employee * $production->employee);
                $sum_x1_x2 += ($production->employee * $production->machine);
                $sum_x1_x3 += ($production->employee * $production->demand);
                $sum_x2_x2 += ($production->machine * $production->machine);
                $sum_x2_x3 += ($production->machine * $production->demand);
                $sum_x3_x3 += ($production->demand * $production->demand);
            }

            $persamaan_1 = new Regression($sum_y, $count_data, $sum_x1, $sum_x2, $sum_x3);
            $persamaan_2 = new Regression($sum_x1_y, $sum_x1, $sum_x1_x1, $sum_x1_x2, $sum_x1_x3);
            $persamaan_3 = new Regression($sum_x2_y, $sum_x2, $sum_x1_x2, $sum_x2_x2, $sum_x2_x3);
            $persamaan_4 = new Regression($sum_x3_y, $sum_x3, $sum_x1_x3, $sum_x2_x3, $sum_x3_x3);

            //menghilangkan alpha persamaan 1 dan 2 (dibagi alpha persamaan 2, kemudian kali alpha persamaan 1)
            $hasil_y = $persamaan_1->getY() - (($persamaan_2->getY()/$persamaan_2->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_alpha = $persamaan_1->getAlpha() - (($persamaan_2->getAlpha()/$persamaan_2->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_1 = $persamaan_1->getBeta1() - (($persamaan_2->getBeta1()/$persamaan_2->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_2 = $persamaan_1->getBeta2() - (($persamaan_2->getBeta2()/$persamaan_2->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_3 = $persamaan_1->getBeta3() - (($persamaan_2->getBeta3()/$persamaan_2->getAlpha()) * $persamaan_1->getAlpha());
            //didapatkan persamaan 5
            $persamaan_5 = new Regression($hasil_y, $hasil_alpha, $hasil_beta_1, $hasil_beta_2, $hasil_beta_3);

            //menghilangkan alpha persamaan 1 dan 3 (dibagi alpha persamaan 3, kemudian kali alpha persamaan 1)
            $hasil_y = $persamaan_1->getY() - (($persamaan_3->getY()/$persamaan_3->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_alpha = $persamaan_1->getAlpha() - (($persamaan_3->getAlpha()/$persamaan_3->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_1 = $persamaan_1->getBeta1() - (($persamaan_3->getBeta1()/$persamaan_3->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_2 = $persamaan_1->getBeta2() - (($persamaan_3->getBeta2()/$persamaan_3->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_3 = $persamaan_1->getBeta3() - (($persamaan_3->getBeta3()/$persamaan_3->getAlpha()) * $persamaan_1->getAlpha());
            //didapatkan persamaan 6
            $persamaan_6 = new Regression($hasil_y, $hasil_alpha, $hasil_beta_1, $hasil_beta_2, $hasil_beta_3);

            //menghilangkan alpha persamaan 1 dan 4 (dibagi alpha persamaan 4, kemudian kali alpha persamaan 1)
            $hasil_y = $persamaan_1->getY() - (($persamaan_4->getY()/$persamaan_4->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_alpha = $persamaan_1->getAlpha() - (($persamaan_4->getAlpha()/$persamaan_4->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_1 = $persamaan_1->getBeta1() - (($persamaan_4->getBeta1()/$persamaan_4->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_2 = $persamaan_1->getBeta2() - (($persamaan_4->getBeta2()/$persamaan_4->getAlpha()) * $persamaan_1->getAlpha());
            $hasil_beta_3 = $persamaan_1->getBeta3() - (($persamaan_4->getBeta3()/$persamaan_4->getAlpha()) * $persamaan_1->getAlpha());
            //didapatkan persamaan 7
            $persamaan_7 = new Regression($hasil_y, $hasil_alpha, $hasil_beta_1, $hasil_beta_2, $hasil_beta_3);

            //menghilangkan beta_1 persamaan 5 dan 6 (dibagi beta_1 persamaan 6, kemudian kali beta_1 persamaan 5)
            $hasil_y = $persamaan_5->getY() - (($persamaan_6->getY()/$persamaan_6->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_alpha = $persamaan_5->getAlpha() - (($persamaan_6->getAlpha()/$persamaan_6->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_beta_1 = $persamaan_5->getBeta1() - (($persamaan_6->getBeta1()/$persamaan_6->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_beta_2 = $persamaan_5->getBeta2() - (($persamaan_6->getBeta2()/$persamaan_6->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_beta_3 = $persamaan_5->getBeta3() - (($persamaan_6->getBeta3()/$persamaan_6->getBeta1()) * $persamaan_5->getBeta1());
            //didapatkan persamaan 8
            $persamaan_8 = new Regression($hasil_y, $hasil_alpha, $hasil_beta_1, $hasil_beta_2, $hasil_beta_3);

            //menghilangkan beta_1 persamaan 5 dan 7 (dibagi beta_1 persamaan 7, kemudian kali beta_1 persamaan 5)
            $hasil_y = $persamaan_5->getY() - (($persamaan_7->getY()/$persamaan_7->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_alpha  = $persamaan_5->getAlpha() - (($persamaan_7->getAlpha()/$persamaan_7->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_beta_1 = $persamaan_5->getBeta1() - (($persamaan_7->getBeta1()/$persamaan_7->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_beta_2 = $persamaan_5->getBeta2() - (($persamaan_7->getBeta2()/$persamaan_7->getBeta1()) * $persamaan_5->getBeta1());
            $hasil_beta_3 = $persamaan_5->getBeta3() - (($persamaan_7->getBeta3()/$persamaan_7->getBeta1()) * $persamaan_5->getBeta1());
            //didapatkan persamaan 9
            $persamaan_9 = new Regression($hasil_y, $hasil_alpha, $hasil_beta_1, $hasil_beta_2, $hasil_beta_3);

            //menghilangkan beta_2 persamaan 8 dan 9 (dibagi beta_2 persamaan 9, kemudian kali beta_2 persamaan 8)
            $hasil_y = $persamaan_8->getY() - (($persamaan_9->getY()/$persamaan_9->getBeta2()) * $persamaan_8->getBeta2());
            $hasil_alpha  = $persamaan_8->getAlpha() - (($persamaan_9->getAlpha()/$persamaan_9->getBeta2()) * $persamaan_8->getBeta2());
            $hasil_beta_1 = $persamaan_8->getBeta1() - (($persamaan_9->getBeta1()/$persamaan_9->getBeta2()) * $persamaan_8->getBeta2());
            $hasil_beta_2 = $persamaan_8->getBeta2() - (($persamaan_9->getBeta2()/$persamaan_9->getBeta2()) * $persamaan_8->getBeta2());
            $hasil_beta_3 = $persamaan_8->getBeta3() - (($persamaan_9->getBeta3()/$persamaan_9->getBeta2()) * $persamaan_8->getBeta2());
            //didapatkan persamaan 10
            $persamaan_10 = new Regression($hasil_y, $hasil_alpha, $hasil_beta_1, $hasil_beta_2, $hasil_beta_3);


            //menentukan beta_3 dari persamaan 10
            $beta_3 = $persamaan_10->getY()/$persamaan_10->getBeta3();
            //menentukan beta_2 dari persamaan 9
            $beta_2 = ($persamaan_9->getY()-($persamaan_9->getBeta3()*$beta_3))/$persamaan_9->getBeta2();
            //menentukan beta_1 dari persamaan 7
            $beta_1 = ($persamaan_7->getY()-($persamaan_7->getBeta2()*$beta_2)-($persamaan_7->getBeta3()*$beta_3))/$persamaan_7->getBeta1();
            //menentukan alpha dari persamaan 1
            $alpha = ($persamaan_1->getY()-($persamaan_1->getBeta1()*$beta_1)-($persamaan_1->getBeta2()*$beta_2)-($persamaan_1->getBeta3()*$beta_3))/$persamaan_1->getAlpha();

            //save alpha
            Setting::set( 'alpha', round($alpha, 4));
            //save beta_1
            Setting::set('beta_1', round($beta_1, 4));
            //save beta_2
            Setting::set('beta_2', round($beta_2, 4));
            //save beta_3
            Setting::set('beta_3', round($beta_3, 4));
            //save setting
            Setting::save();

            //update prediction value
            $productions = Production::all();

            foreach ($productions as $production){
                $prediction = $alpha
                    + ($beta_1 * $production->employee)
                    + ($beta_2 * $production->machine)
                    + ($beta_3 * $production->demand);
                //update to db
                $update_pred = Production::find($production->id);
                $update_pred->prediction = round($prediction);
                $update_pred->save();
            }

            //flash message
            Session::flash('message_regression', 'yes');
            Session::flash('message_header', 'Berhasil!');
            Session::flash('message', 'Model regresi linear berganda berhasil dibuat.');
            Session::flash('icon', 'pe-7s-check');
            Session::flash('class-style', 'alert-success');


        }catch (\ErrorException $e){
            //flash message
            Session::flash('message_regression', 'yes');
            Session::flash('message_header', 'Kesalahan!');
            Session::flash('message', 'Gagal membuat model regresi linear berganda, gunakan jangka tahun lain.');
            Session::flash('icon', 'pe-7s-close-circle');
            Session::flash('class-style', 'alert-danger');
        }catch (\PDOException $e){
            //flash message
            Session::flash('message_regression', 'yes');
            Session::flash('message_header', 'Kesalahan!');
            Session::flash('message', 'Gagal membuat model regresi linear berganda, gunakan jangka tahun lain.');
            Session::flash('icon', 'pe-7s-close-circle');
            Session::flash('class-style', 'alert-danger');
        }

        return redirect('pengaturan');
    }
}
