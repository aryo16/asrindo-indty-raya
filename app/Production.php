<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    protected $fillable = [
        'year',
        'demand',
        'employee',
        'machine',
        'production',
        'prediction'
    ];
}
