@extends('master')

@section('title')
    Data Prediksi dan Produksi
@endsection

@section('css')
    {{--<link href="{{ URL::asset('assets/css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="title">Data Prediksi dan Produksi</h4>
                                    <p class="category">PT Asrindo Indty Raya</p>
                                </div>
                                <div class="col-md-6">
                                    <a class="btn btn-primary pull-right btnModalAddProduction" data-toggle="modal" data-target="#modal_produksi">Tambah Data Produksi</a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            @if(Session::has('message'))
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('alert.alert')
                                    </div>
                                </div>
                            @endif
                            <table class="table table-bordered table-stripped dt_prediksi">
                                <thead>
                                <tr>
                                    <th>Tahun</th>
                                    <th>Karyawan</th>
                                    <th>Mesin</th>
                                    <th>Permintaan</th>
                                    <th>Produksi</th>
                                    <th>Prediksi</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Grafik Produksi dengan Prediksi</h4>
                        </div>
                        <div class="content">
                            <canvas id="canvas_chart_prediction" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="modal_produksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="titleModalProduction">Tambah Data Produksi</h4>
                </div>
                {!! Form::open(['url' => 'predict', 'class'=>'form_production']) !!}
                <input name="_method" type="hidden" class="method">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                {!! Form::label('year', 'Tahun') !!}
                            </div>
                            <div class="col-md-10">
                                {!! Form::number('year', null, ['class' => 'form-control field_year', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                {!! Form::label('employee', 'Karyawan') !!}
                            </div>
                            <div class="col-md-10">
                                {!! Form::number('employee', null, ['class' => 'form-control field_employee', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                {!! Form::label('machine', 'Mesin') !!}
                            </div>
                            <div class="col-md-10">
                                {!! Form::number('machine', null, ['class' => 'form-control field_machine', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                {!! Form::label('demand', 'Permintaan') !!}
                            </div>
                            <div class="col-md-10">
                                {!! Form::number('demand', null, ['class' => 'form-control field_demand', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                {!! Form::label('production', 'Produksi') !!}
                            </div>
                            <div class="col-md-10">
                                {!! Form::number('production', null, ['class' => 'form-control field_production', 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="Submit" class="btn btn-success">Simpan</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{--<script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>--}}
    <script src="{{ URL::asset('assets/js/jquery.dataTables.js') }}" type="text/javascript"></script>
{{--        <script src="{{ URL::asset('assets/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>--}}
    <script src="{{ URL::asset('assets/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.min.js"></script>
    <script>
        $('.dt_prediksi').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/produksi/json',
            columns: [
                {data: 'year', name: 'year'},
                {data: 'employee', name: 'employee'},
                {data: 'machine', name: 'machine'},
                {data: 'demand', name: 'demand'},
                {data: 'production', name: 'production'},
                {data: 'prediction', name: 'prediction'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ]
        });

        $(document).on('click', '.btnDeleteProduction', function(){
            var id_production = $(this).attr('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            swal({
                title : "Hapus data produksi ini?",
                text : "Data produksi yang dihapus tidak dapat dikembalikan!",
                type : "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText : "Hapus!",
                cancelButtonText: "Kembali",
                closeOnCancel: true,
                closeOnConfirm: false
            }, function(isConfirm){
                if(isConfirm){
                    $.ajax({
                        type : "DELETE",
                        url : "/produksi/" + id_production,
                        success: function(data){
                            swal("Terhapus!", "Data produksi berhasil dihapus.", "success");
                            location.reload();
                        },
                        error   : function(){
                            swal('Gagal !', 'Data produksi gagal dihapus.', 'error')
                        }
                    });
                }
            })
        });

        $(document).on('click', '.btnModalEditProduction', function () {
            //mendapatkan id activity
            var id_production = $(this).attr('id');
            //ajax mendapatkan data activity tersebut
            $.get("produksi/"+id_production, function(data){
                $('.form_production').trigger('reset').attr('action', 'produksi/'+id_production);
                $('.method').val('PUT');
                $('#titleModalProduction').text("Ubah Data Produksi");
                $('.field_year').val(data.year);
                $('.field_machine').val(data.machine);
                $('.field_employee').val(data.employee);
                $('.field_demand').val(data.demand);
                $('.field_production').val(data.production);
                $('#modal_produksi').modal('show');
            });
        });

        $(document).on('click', '.btnModalAddProduction', function () {
            $('.form_production').trigger('reset').attr('action', 'produksi');
            $('.method').val('POST');
            $('#modal_produksi').modal('show');
        });

        //chart trend sentiment
        var canvas_sentitrend = document.getElementById("canvas_chart_prediction");
        var config_sentitrend = {
            type: 'line',
            data: {
                labels: ['Tahun'],
                datasets: [{
                    label: 'Label 1',
                    data: [0],
                    backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    lineTension: 0,
                    fill: false
                }, {
                    label: 'Label 0',
                    data: [0],
                    backgroundColor: 'rgba(54, 162, 235, 0.5)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    lineTension: 0,
                    fill: false
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Grafik Produksi dan Prediksi',
                    fontSize: 20
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
//                            max: 10,
                            callback: function (value) {
                                return value
                            },
                            fontSize: 18
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Jumlah Produksi/Prediksi",
                            fontSize: 18
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize: 18
                        }
                    }]
                },
                legend: {
                    labels: {
                        // This more specific font property overrides the global property
                        fontSize: 18
                    }
                }
            }
        };
        var chart_sentitrend = new Chart(canvas_sentitrend, config_sentitrend);
//        canvas_sentitrend.onclick = function (ev) {
//            var activePoints = chart_sentitrend.getElementAtEvent(ev)[0];
//            if (activePoints) {
//                var label = chart_sentitrend.data.labels[activePoints._index];
//                var sentiment = chart_sentitrend.data.datasets[activePoints._datasetIndex].label;
//                // var value = chart_sentitrend.data.datasets[activePoints._datasetIndex].data[activePoints._index];
//                var figure = $('.formFigureSentitrend').val();
//                $.get('json/senti_trend_detail/' + figure + '/' + sentiment + '/' + label, function (data) {
//                    var html = '';
//                    $.each(data, function (key, value) {
//                        html += '<div class="service-box">';
//                        html += '<div class="service-content">';
//                        html += '<h4>'+value.name+' - '+value.province+'</h4>';
//                        html += '<p>'+value.tweet+'</p>';
//                        html += '</div>';
//                        html += '</div>';
//                    });
//                    $('.detail_senti_trend').html(html);
//                    $('.modal_detail_senti_trend').modal('show');
//                });
//            }
//
//        };

        $.get('produksi/chart_data/', function (data) {
            var produksi = data.production;
            var prediksi = data.prediction;
            var year = data.year;
            var label = data.label;
            var title = '';

            //set to chart
            chart_sentitrend.config.data.datasets[0].data = produksi;
            chart_sentitrend.config.data.datasets[1].data = prediksi;
            chart_sentitrend.config.data.datasets[0].label = label[0];
            chart_sentitrend.config.data.datasets[1].label = label[1];
            chart_sentitrend.config.options.title.text = title;
            chart_sentitrend.config.data.labels = year;
            chart_sentitrend.update();
            $('.modal_senti_trend').modal('hide')
        });

    </script>
@endsection