<div class="alert {{ Session::get('class-style') }} alert-with-icon alert-dismissible" data-notify="container">
    <button type="button" class="close" aria-hidden="true">×</button>
    <span data-notify="icon" class="{{ Session::get('icon') }}"></span>
    <span data-notify="message">{{ Session::get('message_header') }}</span>
    <span data-notify="message">{{ Session::get('message') }}</span>
</div>