@extends('master')

@section('css')
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <h1 class="title">Selamat Datang!</h1>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Data Produksi</h4>
                            <p class="category">PT Asrindo Indty Raya</p>
                        </div>
                        <div class="content">
                            <canvas id="canvas_chart_prediction" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.min.js"></script>
    <script>
        //chart trend sentiment
        var canvas_sentitrend = document.getElementById("canvas_chart_prediction");
        var config_sentitrend = {
            type: 'line',
            data: {
                labels: ['Tahun'],
                datasets: [{
                    label: 'Label 1',
                    data: [0],
                    backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    lineTension: 0,
                    fill: false
                }, {
                    label: 'Label 0',
                    data: [0],
                    backgroundColor: 'rgba(54, 162, 235, 0.5)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    lineTension: 0,
                    fill: false
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Grafik Produksi dan Prediksi',
                    fontSize: 20
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
//                            max: 10,
                            callback: function (value) {
                                return value
                            },
                            fontSize: 18
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Jumlah Produksi/Prediksi",
                            fontSize: 18
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize: 18
                        }
                    }]
                },
                legend: {
                    labels: {
                        // This more specific font property overrides the global property
                        fontSize: 18
                    }
                }
            }
        };
        var chart_sentitrend = new Chart(canvas_sentitrend, config_sentitrend);
        //        canvas_sentitrend.onclick = function (ev) {
        //            var activePoints = chart_sentitrend.getElementAtEvent(ev)[0];
        //            if (activePoints) {
        //                var label = chart_sentitrend.data.labels[activePoints._index];
        //                var sentiment = chart_sentitrend.data.datasets[activePoints._datasetIndex].label;
        //                // var value = chart_sentitrend.data.datasets[activePoints._datasetIndex].data[activePoints._index];
        //                var figure = $('.formFigureSentitrend').val();
        //                $.get('json/senti_trend_detail/' + figure + '/' + sentiment + '/' + label, function (data) {
        //                    var html = '';
        //                    $.each(data, function (key, value) {
        //                        html += '<div class="service-box">';
        //                        html += '<div class="service-content">';
        //                        html += '<h4>'+value.name+' - '+value.province+'</h4>';
        //                        html += '<p>'+value.tweet+'</p>';
        //                        html += '</div>';
        //                        html += '</div>';
        //                    });
        //                    $('.detail_senti_trend').html(html);
        //                    $('.modal_detail_senti_trend').modal('show');
        //                });
        //            }
        //
        //        };

        $.get('produksi/chart_data/', function (data) {
            var produksi = data.production;
            var prediksi = data.prediction;
            var year = data.year;
            var label = data.label;
            var title = '';

            //set to chart
            chart_sentitrend.config.data.datasets[0].data = produksi;
            chart_sentitrend.config.data.datasets[1].data = prediksi;
            chart_sentitrend.config.data.datasets[0].label = label[0];
            chart_sentitrend.config.data.datasets[1].label = label[1];
            chart_sentitrend.config.options.title.text = title;
            chart_sentitrend.config.data.labels = year;
            chart_sentitrend.update();
        });
    </script>
@endsection