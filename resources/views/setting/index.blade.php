@extends('master')

@section('title')
    Pengaturan Sistem
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="title">Pengaturan Sistem Prediksi</h4>
                                    <p class="category">PT Asrindo Indty Raya</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="content">
                            @if(Session::has('message_query'))
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('alert.alert')
                                    </div>
                                </div>
                            @else
                                @if(Session::has('message_setting'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('alert.alert')
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! Form::open(['url' => 'pengaturan']) !!}
                                        <div class="form-group">
                                            {!! Form::label('start_year', 'Tahun Mulai Pemodelan :') !!}
                                            {!! Form::select('start_year', $year, $start_year, ['class' => 'form-control start_year', 'required', 'placeholder' => 'Pilih Tahun Awal Pemodelan']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('end_year', 'Tahun Akhir Pemodelan :') !!}
                                            {!! Form::select('end_year', [], null, ['class' => 'form-control end_year', 'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">SIMPAN</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Model Sistem Prediksi</h4>
                            <p class="category">Regresi Linear Berganda</p>
                        </div>
                        <hr>
                        <div class="content">
                            @if(Session::has('message_regression'))
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('alert.alert')
                                    </div>
                                </div>
                            @endif
                            {!! Form::open(['url' => 'regresi/proses']) !!}
                            <button type="submit" class="btn btn-success center-block">Proses Prediksi</button>
                            {!! Form::close() !!}
                            @if(isset($alpha) && isset($beta_1) && isset($beta_2) && isset($beta_3))
                                <h4>Y = {{ $alpha }} + ({{ $beta_1 }}) b1 + ({{ $beta_2 }}) b2 + ({{ $beta_3 }}) b3</h4>
                            @else
                                <h4 class="text-center">Klik tombol "Proses Prediksi" untuk mendapatkan nilai model prediksi.</h4>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('.start_year').on('change', function() {
            $('.end_year').find('option').remove().end();
            //mendapatkan data dari tahun akhir dari tahun awal
            $.get("pengaturan/end_year_list/"+this.value, function(data){
                $('.end_year').append(data);
            });
        });

        var start_year = $('.start_year').find(":selected").text();
        if(start_year != null){
            $.get("pengaturan/end_year_list/"+start_year, function(data){
                $('.end_year').append(data);
            });
        }
    </script>
@endsection