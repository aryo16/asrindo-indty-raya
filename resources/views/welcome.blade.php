<html>
<head>
    <base href="https://demos.creative-tim.com/material-dashboard-pro-angular2/">
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="./assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login | PT Asrindo Indty Raya</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <link rel="stylesheet" href="styles.c9f5b713c291df73f029.css">
    <script charset="utf-8" src="1.f4756e2163045401f11c.js"></script>
    <script charset="utf-8" src="14.b91601a73307b1425ec7.js"></script>
    <script charset="utf-8" src="6.04a1b8214a3e04969821.js"></script>
    <script charset="utf-8" src="5.ea4c42b380ad2f9c2960.js"></script>
    <script charset="utf-8" src="3.424cffd39d55c1cdbbab.js"></script>
</head>
<body class="login-page off-canvas-sidebar">

<div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black"
         style="background-image: url('./assets/img/login.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto">
                    {!! Form::open(['url' => 'login', 'class' => 'form ng-untouched ng-pristine ng-valid']) !!}
                        <div class="card card-login">
                            <div class="card-header card-header-rose text-center">
                                <h4 class="card-title">Log in</h4>
                            </div>
                            <div class="card-body ">
                                <p class="card-description text-center"></p>
                                <span class="bmd-form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">email</i>
                                            </span>
                                        </div>
                                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email...', 'required', 'type' => 'email']) !!}
                                    </div>
                                </span>
                                <span class="bmd-form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                        </div>
                                        {!! Form::password('password', ['class'=>'form-control', 'placeholder' => 'Password...']) !!}
                                    </div>
                                </span>
                            </div>
                            <div class="card-footer justify-content-center">
                                <button type="submit" class="btn btn-rose btn-link btn-lg">LOGIN</button>
                            </div>
                        </div>
                    </form>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <footer class="footer ">
            <div class="container">
                <div class="copyright pull-right"> © 2018, made with <i class="material-icons">favorite</i>
                    by <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better
                    web.
                </div>
            </div>
        </footer>
    </div>
</div>

</body>
</html>